4-Channel NiMH/Li-Ion Battery Tester
====================================

This tester will discharge any combination of up to four NiMH and Li-Ion
cells into a dummy load to determine capacity.  Cell type is determined
automatically upon connection.  A pushbutton rotates through the four
channels, selecting the results to view.  The display is a 1" OLED.  A USB-C
connector is used for power; the AVRISP header pads are used to program the
onboard ATMega328PB microcontroller (and could optionally be used instead of
USB-C for power).

While the default configuration offers testing of NiMH and Li-Ion cells, the
voltage range constants in the software can be adjusted to enable testing of
LiFePO4 cells, lead-acid cells, or any other battery chemistry you might
want to test.

This project is an evolution of the original design at
https://www.instructables.com/id/Li-ion-Capacity-Tester-/.  In addition to
expanding from one channel to four, I also made these changes:

1) The IRFZ44N is a far larger MOSFET than needed.  The AO3416 should be
   more than sufficient, with power dissipation in this application of only
   a few milliwatts.  My experience with the previous version has borne out
   this decision; the AO3416 doesn't even break a sweat. :)
2) This version replaces the 4.7Ω 5W through-hole load resistors with sets of 
   20 95.1Ω ¼W SMD resistors that pencil out to 4.755Ω and the same power. 
   This should facilitate automated manufacturing, and the PCB should be
   better able to absorb and disspiate the heat.
3) The software was cleaned up a bit to better manage transitions between
   different states: idle (no battery), discharging, discharged, and error 
   (voltage too low or high)
4) The speaker beeps on startup, and beeps when a cell is discharged.  It
   plays a lower tone on error.
5) SPI is used to communicate with the OLED, instead of I2C.  The particular
   display board Amazon sent me uses the SSD1306 controller, so that needed
   changing as well.

The software was imported into PlatformIO on VSCodium for enhancement.  If
desired, you could rename software.cpp to software.ino, remove the Arduino.h
#include, and build under the Arduino IDE if desired.
