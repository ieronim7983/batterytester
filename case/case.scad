w=90;
d=82;
h=24;
t=2;
s=42;
$fn=30;

translate([-96,141,9])
%import("../BatteryTester.stl");

%bottom();
%top();
button();

module bottom()
{
    // bottom
    translate([.5,.5,.5])
    minkowski()
    {
        cube([w-1,d-1,t-1]);
        sphere(d=1);
    }

    // standoffs
    translate([12,6,t])
        cylinder(d=5, h=5.25);
    translate([28,6,t])
        cylinder(d=5, h=5.25);
    translate([47,6,t])
        cylinder(d=5, h=5.25);
    translate([65,6,t])
        cylinder(d=5, h=5.25);
    translate([81,6,t])
        cylinder(d=5, h=5.25);
    translate([65,60,t])
        cylinder(d=5, h=5.25);
    translate([25,60,t])
        cylinder(d=5, h=5.25);
    translate([5,d-5,t])
    difference()
    {
        cylinder(d=5.5, h=h-2*t);
        translate([0,0,h-2*t-10])
            cylinder(d=3.1, h=10);
    }
    translate([w-5,d-5,t])
    difference()
    {
        cylinder(d=5.5, h=h-2*t);
        translate([0,0,h-2*t-10])
            cylinder(d=3.1, h=10);
    }
    translate([5,s+6,t])
    difference()
    {
        cylinder(d=5.5, h=h-2*t);
        translate([0,0,h-2*t-10])
            cylinder(d=3.1, h=10);
    }
    translate([w-5,s+6,t])
    difference()
    {
        cylinder(d=5.5, h=h-2*t);
        translate([0,0,h-2*t-10])
            cylinder(d=3.1, h=10);
    }

    // left side

    translate([.5,.5,.5])
    {
        minkowski()
        {
            cube([t-1,d-1,10]);
            sphere(d=1);
        }
        translate([6,0,0])
        minkowski()
        {
            cube([t-1,d-1,8.5]);
            sphere(d=1);
        }
		difference()
		{
			translate([0,s,0])
			minkowski()
			{
				cube([t-1,d-s-1,h-3]);
				sphere(d=1);
			}
			translate([-2,56.5,12])
			cube([4,12,7]);
		}
    }

    // right side

    translate([.5+w-2,.5,.5])
    {
        minkowski()
        {
            cube([t-1,d-1,10]);
            sphere(d=1);
        }
        translate([-6,0,0])
        minkowski()
        {
            cube([t-1,d-1,8.5]);
            sphere(d=1);
        }
        translate([0,s,0])
        minkowski()
        {
            cube([t-1,d-s-1,h-3]);
            sphere(d=1);
        }
    }

	// front

    translate([.5,.5,.5])
    minkowski()
    {
        cube([w-1,1,8.5]);
        sphere(d=1);
    }

    // back

    translate([.5,d-1.5,.5])
    minkowski()
    {
        cube([w-1,1,h-3]);
        sphere(d=1);
    }

}

module top()
{
    difference()
    {
        translate([.5,s+.5,h-1.5])
        minkowski()
        {
            cube([w-1,d-s-1,t-1]);
            sphere(d=1);
        }

        // holes

        translate([5,d-5,h-2])
            cylinder(d=3.4, h=2);
        translate([w-5,d-5,h-2])
            cylinder(d=3.4, h=2);
        translate([5,s+6,h-2])
            cylinder(d=3.4, h=2);
        translate([w-5,s+6,h-2])
            cylinder(d=3.4, h=2);

        // window

        translate([14.5,57,h-2])
            cube([26.5,15.5,2]);

        // button hole
    
        translate([63.5,69.25,h-2])
            cylinder(d=4.3, h=2);
    }
        
    // button guide

    translate([63.5,69.25,15.35])
    difference()
    {   
        cylinder(d=6, h=h-17.35);
        cylinder(d=4.3, h=h-17.35);
    }

    // front

    difference()
    {
        translate([2.5,s+.5,10])
        minkowski()
        {
            cube([w-5,t-1,h-10.5]);
            sphere(d=1);
        }

        // MOSFET clearance

        translate([17.6,s,9.5])
            cube([3.8,2,2]);
        translate([36.6,s,9.5])
            cube([3.8,2,2]);
        translate([54.5,s,9.5])
            cube([3.8,2,2]);
        translate([72.2,s,9.5])
            cube([3.8,2,2]);
    }
}

module button()
{
    translate([63.5,69.25,14.25])
    {   
        cylinder(d=6, h=1);
        cylinder(d=3.5, h=h-12.5);
    }
}